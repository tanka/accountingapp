package com.tanka.bs.accounts.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

@Entity
public class PaymentDetails {

	@Id 
	@TableGenerator(name="payid", table="paypktb", pkColumnName="paypk", 
		pkColumnValue="payvalue", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="payid")
	private long id;

	@ManyToOne
	private Students student;
	private Date paymentDate;
	private Float paidAmount;

	public PaymentDetails(Students std, String string,
			String amt, Date feesEntryDate) {
		this.student = std;
		this.paymentDate = feesEntryDate;
		this.paidAmount = Float.parseFloat(amt);
	}

	public PaymentDetails() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Students getStudent() {
		return student;
	}

	public void setStudent(Students student) {
		this.student = student;
	}

	public Date getDate() {
		return paymentDate;
	}

	public void setDate(Date date) {
		this.paymentDate = date;
	}

	public Float getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Float paidAmount) {
		this.paidAmount = paidAmount;
	}
}