package com.tanka.bs.accounts.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;

@Entity
public class Classes {
	
	@Id
	@TableGenerator(name="clsid", table="clspktb", pkColumnName="clspk", 
			pkColumnValue="clsvalue", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="clsid")
	private long id;
	private String classTeacher;
	private String class_;
	
	@OneToOne
	Subjects subject;
	Float fees;
	
	public Classes() {
	}
	
	public Classes(String classTeacher, String class_, float fees){
		this.classTeacher = classTeacher;
		this.class_ = class_;
		this.fees = fees;
	}
	
	public Classes(String classTeacher, String class_, Float fees, Subjects sub){}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getClassTeacher() {
		return classTeacher;
	}
	public void setClassTeacher(String classTeacher) {
		this.classTeacher = classTeacher;
	}
	public Subjects getSubject() {
		return subject;
	}
	public void setSubject(Subjects subject) {
		this.subject = subject;
	}
	public Float getFees() {
		return fees;
	}
	public void setFees(Float fees) {
		fees = fees;
	}
	public String getClass_() {
		return class_;
	}
	public void setClass_(String class_) {
		this.class_ = class_;
	}
	

}
