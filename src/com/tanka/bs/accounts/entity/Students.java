package com.tanka.bs.accounts.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cascade;

@Entity
public class Students {
	@Id
	String rollNum;
	String name;
	
	@ManyToOne(fetch=FetchType.EAGER)
	Classes class_;
	String fathersName;
	String mothersName;
	String localGuide;
	String phoneNumber;
	String address;
	
	public Students() {
	
	}
	public Students(String rollNumber, String name2, Classes classes,
			String fathersName2, String mothersName2, String localGuide2,
			String phone, String address2) {
		this.rollNum = rollNumber;
		this.name = name2;
		this.class_ = classes;
		this.fathersName = fathersName2;
		this.mothersName = mothersName2;
		this.localGuide = localGuide2;
		this.phoneNumber = phone;
		this.address = address2;
	}
	public String getRollNum() {
		return rollNum;
	}
	public void setRollNum(String rollNum) {
		this.rollNum = rollNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Classes getClass_() {
		return class_;
	}
	public void setClass_(Classes class_) {
		this.class_ = class_;
	}
	public String getFathersName() {
		return fathersName;
	}
	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}
	public String getMothersName() {
		return mothersName;
	}
	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}
	public String getLocalGuide() {
		return localGuide;
	}
	public void setLocalGuide(String localGuide) {
		this.localGuide = localGuide;
	}
	public String getPhonenumber() {
		return phoneNumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phoneNumber = phonenumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}