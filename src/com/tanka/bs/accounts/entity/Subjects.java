package com.tanka.bs.accounts.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Subjects {
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;

	private String math; 
	private String english;
	private String physics;
	private String history;
	private String chemistry;
	private String biology;
	private String geography;
	private String socialStudies;
	private String dzongkha;
	private String nepali;
	private String hindi;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMath() {
		return math;
	}
	public void setMath(String math) {
		this.math = math;
	}
	public String getEnglish() {
		return english;
	}
	public void setEnglish(String english) {
		this.english = english;
	}
	public String getPhysics() {
		return physics;
	}
	public void setPhysics(String physics) {
		this.physics = physics;
	}
	public String getHistory() {
		return history;
	}
	public void setHistory(String history) {
		this.history = history;
	}
	public String getChemistry() {
		return chemistry;
	}
	public void setChemistry(String chemistry) {
		this.chemistry = chemistry;
	}
	public String getBiology() {
		return biology;
	}
	public void setBiology(String biology) {
		this.biology = biology;
	}
	public String getGeography() {
		return geography;
	}
	public void setGeography(String geography) {
		this.geography = geography;
	}
	public String getSocialStudies() {
		return socialStudies;
	}
	public void setSocialStudies(String socialStudies) {
		this.socialStudies = socialStudies;
	}
	public String getDzongkha() {
		return dzongkha;
	}
	public void setDzongkha(String dzongkha) {
		this.dzongkha = dzongkha;
	}
	public String getNepali() {
		return nepali;
	}
	public void setNepali(String nepali) {
		this.nepali = nepali;
	}
	public String getHindi() {
		return hindi;
	}
	public void setHindi(String hindi) {
		this.hindi = hindi;
	}
	
}
