package com.tanka.bs.accounts.entity.query;

import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.tanka.bs.accounts.HibernateUtil;
import com.tanka.bs.accounts.entity.Classes;
import com.tanka.bs.accounts.entity.Students;

public class StudentsQuery {
	public static Students putStudentToDb(Students student) {
		Session s = HibernateUtil.getSession();
		org.hibernate.Transaction tx = null;
		try {
			tx = s.beginTransaction();

			// here get object
			@SuppressWarnings("unchecked")
			List<Students> list = s.createCriteria(Students.class).list();
			if (!list.isEmpty()) {

				for (Iterator<Students> iterator = list.iterator(); iterator
						.hasNext();) {
					Students student_ = iterator.next();
					if (student_.getRollNum().equalsIgnoreCase(
							student.getRollNum())) {								
						JOptionPane.showMessageDialog(
								null,
								"Student with Roll Number "
										+ student.getRollNum()
										+ "is already present");
						
						s.close();
						return null;
					}
				}
				s.save(student);
				tx.commit();
	
				return student;

			} else {// enter student to db here
				s.save(student);
				tx.commit();
		//		s.close();
				return student;
			}

		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			Logger.getLogger("con").info("Exception: " + ex.getMessage());
			ex.printStackTrace(System.err);
		} finally {
			
		}
		return null;

	}

	public static List<Students> getStudentsListForClasses(Classes cls) {
		Session s = HibernateUtil.getSession();
		org.hibernate.Transaction tx = null;
		try {
			tx = s.beginTransaction();
			
			Criteria cr = s.createCriteria(Students.class, "STUDENTS");
			// TODO to create a warning for null value.
			cr.add(Restrictions.eq("class_.id",cls.getId()));
			@SuppressWarnings("unchecked")
			List<Students> list = cr.list();
			if (list.isEmpty()) {
				return null;
			} else {
				return list;
			}
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			Logger.getLogger("con").info("Exception: " + ex.getMessage());
			ex.printStackTrace(System.err);
		} finally {
			
		}
		return null;
	}
}