package com.tanka.bs.accounts.entity.query;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.tanka.bs.accounts.HibernateUtil;
import com.tanka.bs.accounts.entity.Classes;

public class ClassQuery {
	public static Classes getClass(String class_) {
		 Session s = HibernateUtil.getSession();
		    org.hibernate.Transaction tx = null;
		    try {
		        tx = s.beginTransaction();
		        // here get object
		        @SuppressWarnings("unchecked")
				List<Classes> list = s.createCriteria(Classes.class).list();
		        if(list.isEmpty()){		        	
		        	return null;
		        }else {
		        	for (Iterator<Classes> iterator = list.iterator(); iterator
							.hasNext();) {
						Classes classes = (Classes) iterator.next();
						if(classes.getClass_().equals(class_)){
							tx.commit();
							return classes;
						}
					}
		        	tx.commit();
		        	return null;
		        }
		    } catch (HibernateException ex) {
		        if (tx != null) {
		            tx.rollback();
		        }            
		        Logger.getLogger("con").info("Exception: " + ex.getMessage());
		        ex.printStackTrace(System.err);
		    } finally {
		        
		    }
			return null;
	}

	public static long getClassId(String selectedClass) {
		return 0;
	}
}