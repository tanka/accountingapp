package com.tanka.bs.accounts.entity.query;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.tanka.bs.accounts.HibernateUtil;
import com.tanka.bs.accounts.entity.PaymentDetails;
import com.tanka.bs.accounts.entity.utill.ResultsDisplay;

public class PaymentDetailsQuery {

	public static PaymentDetails putPaymentToDb(PaymentDetails paymentDetails) {
		Session s = HibernateUtil.getSession();
		org.hibernate.Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.save(paymentDetails);
			tx.commit();
			return paymentDetails;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			Logger.getLogger("con").info("Exception: " + ex.getMessage());
			ex.printStackTrace(System.err);
		} finally {
		}
		return null;
	}

	static private Vector<String> columns;
	static private Vector<Vector<String>> datas;
	public static void view() {
		getPaymentDetails();
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				
				/*String[] columnNames = { "First Name", "Last Name", "Sport",
						"# of Years", "Vegetarian" };

				Object[][] data = {
						{ "Kathy", "Smith", "Snowboarding", new Integer(5),
								new Boolean(false) },
						{ "John", "Doe", "Rowing", new Integer(3), new Boolean(true) },
						{ "Sue", "Black", "Knitting", new Integer(2),
								new Boolean(false) },
						{ "Jane", "White", "Speed reading", new Integer(20),
								new Boolean(true) },
						{ "Joe", "Brown", "Pool", new Integer(10), new Boolean(false) } };*/
				ResultsDisplay.createAndShowGUI(columns, datas);
			}
		});
	}
	
	public static Object[][] getPaymentDetails() {
		 Session s = HibernateUtil.getSession();
		    org.hibernate.Transaction tx = null;
		    try {
		        tx = s.beginTransaction();
		        // here get object
		        @SuppressWarnings("unchecked")
				List<PaymentDetails> list = s.createCriteria(PaymentDetails.class).list();
		        if(list.isEmpty()){		        	
		        	return null;
		        }else {
		        	columns = new Vector<String>();
		        	datas = new Vector<Vector<String>>();
		        	columns.add("Roll Number");
		        	columns.add("Date");
		        	columns.add("Amount");
		        	
		        	
		        	for (Iterator<PaymentDetails> iterator = list.iterator(); iterator
							.hasNext();) {
		        		PaymentDetails paymentDetail = (PaymentDetails) iterator.next();
		        		
		        		Vector<String> temp = new Vector<String>();
		        		temp.add(paymentDetail.getStudent().getRollNum());
		        		temp.add(paymentDetail.getDate().toString());
		        		temp.add(paymentDetail.getPaidAmount().toString());
		        		datas.add(temp);
					}
		        	tx.commit();
		        	return null;
		        }
		    } catch (HibernateException ex) {
		        if (tx != null) {
		            tx.rollback();
		        }            
		        Logger.getLogger("con").info("Exception: " + ex.getMessage());
		        ex.printStackTrace(System.err);
		    } finally {		        
		    }
			return null;
	}
}
