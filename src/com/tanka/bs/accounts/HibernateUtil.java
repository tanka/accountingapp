package com.tanka.bs.accounts;
 
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import com.tanka.bs.accounts.entity.Classes;
import com.tanka.bs.accounts.entity.PaymentDetails;
import com.tanka.bs.accounts.entity.Students;
import com.tanka.bs.accounts.entity.Subjects;
import com.tanka.bs.accounts.init.InitiliseTableInitiliser;
 
public class HibernateUtil {
	static AnnotationConfiguration config = new AnnotationConfiguration();
	public static boolean setupDatabaseConnection() {
		
		//adding classes for creating tables
		config.addAnnotatedClass(Students.class);
		config.addAnnotatedClass(Classes.class);
		config.addAnnotatedClass(Subjects.class);
		config.addAnnotatedClass(PaymentDetails.class);
		config.configure("hibernate.cfg.xml");
		
		new SchemaExport(config).create(true, false);
		//InitiliseTableInitiliser.run();
		return true;
	}

	static Session session;
	public static Session getSession() {
		SessionFactory factory = config.buildSessionFactory();
		session = factory.getCurrentSession(); 
		return session;
	}
}