package com.tanka.bs.accounts.init;

import org.hibernate.Session;

import com.tanka.bs.accounts.HibernateUtil;
import com.tanka.bs.accounts.entity.Classes;
import com.tanka.bs.accounts.entity.Students;

public class InitiliseTableInitiliser {
	public static void run() {
		Session s = HibernateUtil.getSession();
		org.hibernate.Transaction tx = null;
		tx = s.beginTransaction();

		Classes class_1 = new Classes("Tanka", "Class 1", (float) 1000.0);
		Classes class_2 = new Classes("Tanka2", "Class 2", (float) 1000.0);
		Classes class_3 = new Classes("Tanka3", "Class 3", (float) 1000.0);
		Classes class_4 = new Classes("Tanka4", "Class 4", (float) 1000.0);
		Classes class_5 = new Classes("Tanka5", "Class 5", (float) 1000.0);
		Classes class_6 = new Classes("Tanka6", "Class 6", (float) 1000.0);
		Classes class_7 = new Classes("Tanka7", "Class 7", (float) 1000.0);
		Classes class_8 = new Classes("Tanka8", "Class 8", (float) 1000.0);
		Classes class_9 = new Classes("Tanka9", "Class 9", (float) 1000.0);
		Classes class_10 = new Classes("Tanka10", "Class 10", (float) 1000.0);

		s.save(class_1);
		s.save(class_2);
		s.save(class_3);
		s.save(class_4);
		s.save(class_5);
		s.save(class_6);
		s.save(class_7);
		s.save(class_8);
		s.save(class_9);
		s.save(class_10);
		tx.commit();
		

		Students student_1 = new Students("IEC2007001", "Name 1", class_1,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_2 = new Students("IEC2007002", "Name 1", class_2,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_3 = new Students("IEC2007003", "Name 1", class_3,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_4 = new Students("IEC2007004", "Name 1", class_4,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_5 = new Students("IEC2007005", "Name 1", class_5,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_6 = new Students("IEC2007006", "Name 1", class_6,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_7 = new Students("IEC2007007", "Name 1", class_7,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_8 = new Students("IEC2007008", "Name 1", class_1,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_9 = new Students("IEC2007009", "Name 1", class_1,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_10 = new Students("IEC20070010", "Name 1", class_2,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_11 = new Students("IEC20070011", "Name 1", class_2,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		Students student_12 = new Students("IEC20070012", "Name 1", class_10,
				"Father_1", "Mother_1", "LocalGuide_1", "1789382",
				"Thimphu Babesa Techpark");
		
		s= HibernateUtil.getSession();
		tx = s.beginTransaction();
		s.save(student_1);
		s.save(student_2);
		s.save(student_3);
		s.save(student_4);
		s.save(student_5);
		s.save(student_6);
		s.save(student_7);
		s.save(student_8);
		s.save(student_9);
		s.save(student_10);
		s.save(student_11);
		s.save(student_12);
		tx.commit();
		
	}
}
