package com.tanka.bs.accounts;

import java.awt.CardLayout;
import java.awt.Choice;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.hibernate.Session;

import com.tanka.bs.accounts.entity.Classes;
import com.tanka.bs.accounts.entity.PaymentDetails;
import com.tanka.bs.accounts.entity.Students;
import com.tanka.bs.accounts.entity.query.ClassQuery;
import com.tanka.bs.accounts.entity.query.PaymentDetailsQuery;
import com.tanka.bs.accounts.entity.query.StudentsQuery;
import javax.swing.JCheckBox;
import javax.swing.JTable;

public class Window {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textFieldFees;
	private JTextField textFieldDate;
	private JTextArea textAddress;

	private JPanel accountViewSelectPanel;
	private JPanel accountEntryPanel;
	JPanel accountViewPanel;
	JPanel welcomePanel;
	JPanel studentEntryPanel;
	private JTextField textRollNum;
	private JTextField textName;
	private JTextField txtFatherName;
	private JTextField txtMothersName;
	private JTextField txtLocalGuide;
	private JTextField txtPhone;
	private JButton btnPaid;

	private Choice choiceClasses;
	JCheckBox chckbxOrViewAll;
	private String selectedClass;
	private Choice choiceStudentRoll;
	private Date feesEntryDate;
	private Students feesEntryStudentSelected;
	private List<Students> feesEntrystudentsList;

	static Session session;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					Window window = new Window();
					window.frame.setVisible(true);
					HibernateUtil.setupDatabaseConnection();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit
				.getDefaultToolkit()
				.getImage(
						"C:\\Users\\Tanka\\MyWork\\HerokuPrjs\\herokuTest\\jettytestyooo123\\src\\main\\webapp\\images\\logo.png"));
		frame.setBounds(100, 100, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));

		welcomePanel = new JPanel();
		frame.getContentPane().add(welcomePanel, "name_6500761853290");
		welcomePanel.setLayout(null);
		welcomePanel.setVisible(true);

		JButton btnAccountsViewSelect = new JButton("View Accounts");
		btnAccountsViewSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accountViewSelectPanel.setVisible(true);
				welcomePanel.setVisible(false);
			}
		});
		btnAccountsViewSelect.setBounds(62, 118, 133, 23);
		welcomePanel.add(btnAccountsViewSelect);

		JButton btnAccountsEntry = new JButton("EnterAccounts");
		btnAccountsEntry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accountEntryPanel.setVisible(true);
				welcomePanel.setVisible(false);
			}
		});
		btnAccountsEntry.setBounds(241, 118, 138, 23);
		welcomePanel.add(btnAccountsEntry);

		JButton btnEnterNewStd = new JButton("Enter New Student");
		btnEnterNewStd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				studentEntryPanel.setVisible(true);
				welcomePanel.setVisible(false);
			}
		});
		btnEnterNewStd.setBounds(62, 407, 133, 23);
		welcomePanel.add(btnEnterNewStd);

		accountViewSelectPanel = new JPanel();
		frame.getContentPane()
				.add(accountViewSelectPanel, "name_6519140278423");
		accountViewSelectPanel.setLayout(null);
		accountViewSelectPanel.setVisible(false);

		textField = new JTextField();
		textField.setBounds(48, 74, 138, 20);
		accountViewSelectPanel.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(228, 74, 138, 20);
		accountViewSelectPanel.add(textField_1);
		textField_1.setColumns(10);

		JLabel lblNewLabel = new JLabel("Enter Class");
		lblNewLabel.setBounds(48, 61, 86, 14);
		accountViewSelectPanel.add(lblNewLabel);

		JLabel lblEnterStudentName = new JLabel("Enter Student Name");
		lblEnterStudentName.setBounds(228, 61, 138, 14);
		accountViewSelectPanel.add(lblEnterStudentName);

		chckbxOrViewAll = new JCheckBox("Or view all accounts");
		chckbxOrViewAll.setBounds(161, 172, 205, 23);
		accountViewSelectPanel.add(chckbxOrViewAll);
		
		JButton btnNewButton = new JButton("View Accounts");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			/*	accountViewPanel.setVisible(true);
				accountViewSelectPanel.setVisible(false);
			*/	
				if(chckbxOrViewAll.isSelected()){
					PaymentDetailsQuery.view();
					
				}
			}
		});
		btnNewButton.setBounds(87, 252, 256, 23);
		accountViewSelectPanel.add(btnNewButton);

		JButton btnNewBack1 = new JButton("Back");
		btnNewBack1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				welcomePanel.setVisible(true);
				accountViewSelectPanel.setVisible(false);
			}
		});
		btnNewBack1.setBounds(10, 11, 86, 39);
		accountViewSelectPanel.add(btnNewBack1);
		
		

		accountViewPanel = new JPanel();
		frame.getContentPane().add(accountViewPanel, "name_6534931109183");
		accountViewPanel.setLayout(null);
		accountViewPanel.setVisible(false);

		JButton button = new JButton("Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accountViewSelectPanel.setVisible(true);
				accountViewPanel.setVisible(false);
			}
		});
		button.setBounds(10, 11, 83, 39);
		accountViewPanel.add(button);
		
		table = new JTable();
		table.setBounds(476, 451, -466, -386);
		accountViewPanel.add(table);

		accountEntryPanel = new JPanel();
		frame.getContentPane().add(accountEntryPanel, "name_6521653898905");
		accountEntryPanel.setLayout(null);
		accountEntryPanel.setVisible(false);

		JLabel lblClass = new JLabel("Class :");
		lblClass.setBounds(32, 55, 46, 14);
		accountEntryPanel.add(lblClass);

		JLabel lblStdRoll = new JLabel("Student RollNumber :");
		lblStdRoll.setBounds(32, 80, 102, 14);
		accountEntryPanel.add(lblStdRoll);

		JLabel lblFees = new JLabel("Fees Amount :");
		lblFees.setBounds(32, 105, 86, 14);
		accountEntryPanel.add(lblFees);

		JLabel lblDate = new JLabel("Date :");
		lblDate.setBounds(32, 130, 46, 14);
		accountEntryPanel.add(lblDate);

		choiceClasses = new Choice();
		choiceClasses.setBounds(183, 49, 156, 20);
		choiceClasses.addItem("Class 1");
		choiceClasses.addItem("Class 2");
		choiceClasses.addItem("Class 3");
		choiceClasses.addItem("Class 4");
		choiceClasses.addItem("Class 5");
		choiceClasses.addItem("Class 6");
		choiceClasses.addItem("Class 7");
		choiceClasses.addItem("Class 8");
		choiceClasses.addItem("Class 9");
		choiceClasses.addItem("Class 10");
		accountEntryPanel.add(choiceClasses);

		choiceClasses.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				selectedClass = choiceClasses.getSelectedItem() + "";
				Classes cls = ClassQuery.getClass(selectedClass);

				feesEntrystudentsList = StudentsQuery
						.getStudentsListForClasses(cls);
				if (feesEntrystudentsList == null
						|| feesEntrystudentsList.isEmpty()) {
					JOptionPane.showMessageDialog(null, "* No Students for "
							+ cls.getClass_());
				} else {
					choiceStudentRoll.removeAll();
					for (Iterator iterator = feesEntrystudentsList.iterator(); iterator
							.hasNext();) {
						Students students = (Students) iterator.next();
						choiceStudentRoll.add(students.getRollNum());
					}
					textFieldFees.setText(cls.getFees().toString());
					feesEntryDate = new Date();
					textFieldDate.setText(feesEntryDate.toString());
				}
			}
		});

		choiceStudentRoll = new Choice();
		choiceStudentRoll.setBounds(183, 75, 156, 20);
		accountEntryPanel.add(choiceStudentRoll);

		btnPaid = new JButton("PAID");
		btnPaid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Students std = null;
				for (Iterator<Students> itr = feesEntrystudentsList.iterator(); itr
						.hasNext();) {
					Students student = (Students) itr.next();
					if (student.getRollNum().equals(
							choiceStudentRoll.getSelectedItem())) {
						std = student;
					}

				}
				PaymentDetailsQuery.putPaymentToDb(new PaymentDetails(std,
						choiceStudentRoll.getSelectedItem(), textFieldFees
								.getText(), feesEntryDate));
				
				textFieldFees.setText("");
				textFieldDate.setText("");
				btnPaid.disable();
			}
		});
		btnPaid.setBounds(32, 181, 198, 23);
		accountEntryPanel.add(btnPaid);

		textFieldFees = new JTextField();
		textFieldFees.setColumns(10);
		textFieldFees.setBounds(183, 102, 156, 20);
		accountEntryPanel.add(textFieldFees);

		textFieldDate = new JTextField();
		textFieldDate.setColumns(10);
		textFieldDate.setBounds(183, 127, 156, 20);
		accountEntryPanel.add(textFieldDate);

		JButton button_1 = new JButton("Back");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				welcomePanel.setVisible(true);
				accountEntryPanel.setVisible(false);
			}
		});
		button_1.setBounds(10, 11, 86, 39);
		accountEntryPanel.add(button_1);

		// Student details entry
		studentEntryPanel = new JPanel();
		frame.getContentPane().add(studentEntryPanel, "name_15166674328499");
		studentEntryPanel.setLayout(null);

		JLabel lblRollnumber = new JLabel("RollNumber* :");
		lblRollnumber.setBounds(27, 28, 79, 14);
		studentEntryPanel.add(lblRollnumber);

		textRollNum = new JTextField();
		textRollNum.setBounds(134, 25, 86, 20);
		studentEntryPanel.add(textRollNum);
		textRollNum.setColumns(10);

		JLabel lblName = new JLabel("Name* :");
		lblName.setBounds(27, 59, 79, 14);
		studentEntryPanel.add(lblName);

		textName = new JTextField();
		textName.setColumns(10);
		textName.setBounds(134, 56, 86, 20);
		studentEntryPanel.add(textName);

		JLabel lbl = new JLabel("Class* :");
		lbl.setBounds(27, 90, 79, 14);
		studentEntryPanel.add(lbl);

		JLabel lblFatherName = new JLabel("Fathers Name :");
		lblFatherName.setBounds(27, 121, 79, 14);
		studentEntryPanel.add(lblFatherName);

		txtFatherName = new JTextField();
		txtFatherName.setColumns(10);
		txtFatherName.setBounds(134, 118, 86, 20);
		studentEntryPanel.add(txtFatherName);

		JLabel lblMothersName = new JLabel("Mothers Name :");
		lblMothersName.setBounds(27, 152, 79, 14);
		studentEntryPanel.add(lblMothersName);

		txtMothersName = new JTextField();
		txtMothersName.setColumns(10);
		txtMothersName.setBounds(134, 149, 86, 20);
		studentEntryPanel.add(txtMothersName);

		JLabel lblLocalGuide = new JLabel("LocalGuide :");
		lblLocalGuide.setBounds(27, 183, 79, 14);
		studentEntryPanel.add(lblLocalGuide);

		txtLocalGuide = new JTextField();
		txtLocalGuide.setColumns(10);
		txtLocalGuide.setBounds(134, 180, 86, 20);
		studentEntryPanel.add(txtLocalGuide);

		JLabel lblPhone = new JLabel("Phone Number :");
		lblPhone.setBounds(27, 214, 79, 14);
		studentEntryPanel.add(lblPhone);

		txtPhone = new JTextField();
		txtPhone.setColumns(10);
		txtPhone.setBounds(134, 211, 86, 20);
		studentEntryPanel.add(txtPhone);

		textAddress = new JTextArea();
		textAddress.setBounds(134, 242, 176, 129);
		studentEntryPanel.add(textAddress);

		final Choice choiceClass = new Choice();
		choiceClass.setBounds(134, 90, 86, 20);
		choiceClass.addItem("Class 1");
		choiceClass.addItem("Class 2");
		choiceClass.addItem("Class 3");
		choiceClass.addItem("Class 4");
		choiceClass.addItem("Class 5");
		choiceClass.addItem("Class 6");
		choiceClass.addItem("Class 7");
		choiceClass.addItem("Class 8");
		choiceClass.addItem("Class 9");
		choiceClass.addItem("Class 10");
		studentEntryPanel.add(choiceClass);
		// Button to add students to db
		JButton btnNewButton_1 = new JButton("Enter New Student");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get values from all the parameters
				String class_ = choiceClass.getSelectedItem();
				String rollNumber = textRollNum.getText();
				String name = textName.getText();
				String fathersName = txtFatherName.getText();
				String mothersName = txtMothersName.getText();
				String localGuide = txtLocalGuide.getText();
				String phone = txtPhone.getText();
				String address = textAddress.getText();
				// check for empty string passed
				if (class_.equals("") || rollNumber.equals("")
						|| name.equals("") /*
											 * || fathersName.equals("") ||
											 * mothersName.equals("") ||
											 * localGuide.equals("") ||
											 * phone.equals("") ||
											 * address.equals("")
											 */) {
					JOptionPane.showMessageDialog(null,
							"* marked fields are necessary");
				} else {
					// enter to db here
					Classes classes = ClassQuery.getClass(class_);
					Students student = new Students(rollNumber, name, classes,
							fathersName, mothersName, localGuide, phone,
							address);
					StudentsQuery.putStudentToDb(student);

				}

				// clear the textfields

			}
		});
		btnNewButton_1.setBounds(134, 382, 131, 69);
		studentEntryPanel.add(btnNewButton_1);

		JLabel lblAddress = new JLabel("Address :");
		lblAddress.setBounds(27, 242, 79, 14);
		studentEntryPanel.add(lblAddress);

		JButton btnEnterStudentToHome = new JButton("Go To Home Screen");
		btnEnterStudentToHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				welcomePanel.setVisible(true);
				studentEntryPanel.setVisible(false);
			}
		});
		btnEnterStudentToHome.setBounds(314, 24, 149, 54);
		studentEntryPanel.add(btnEnterStudentToHome);
	}
}
